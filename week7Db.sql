create schema if not exists bank;
use bank;

drop table if exists branch;
create table branch (
	branch_name varchar(12) not null,
    branch_city varchar(12) not null,
    assets int,
    primary key (branch_name)
);

drop table if exists account;
create table account (
  account_number varchar(5) not null,
  branch_name varchar(12),
  balance int not null,
  primary key (account_number)
);

drop table if exists customer;
create table customer (
  customer_name varchar(15) not null,
  customer_street varchar(12) not null,
  customer_city varchar(12) not null,
  primary key (customer_name)
);

drop table if exists depositor;
create table depositor (
  customer_name varchar(15) not null,
  account_number varchar(5) not null,
  primary key (customer_name, account_number));

drop table if exists borrower;
create table borrower (
  customer_name varchar(12) not null,
  loan_number varchar(4) not null,
  primary key (customer_name, loan_number)
);

drop table if exists loan;
create table loan (
  loan_number varchar(4) not null,
  branch_name varchar(12) not null,
  TLFV int,
  primary key (loan_number)
);
